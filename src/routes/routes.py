#!/usr/bin/python3
# -*- coding: utf-8 -*-

from app import app

# Controllers
# Errors
import src.controller.errors as errors

from src.controller.healthycheck import healthycheck
from src.controller.home import home

app.register_blueprint(healthycheck)
app.register_blueprint(home)

@app.after_request
def add_header(response):
    return response

app.register_error_handler(400, errors.bad_request)
app.register_error_handler(404, errors.not_found_error)
app.register_error_handler(500, errors.internal_error)
