#!/usr/bin/python3
# -*- coding: utf-8 -*-

from src.handler.response import response_json
import src.handler.response as resp

from src.utils import logger
from config import environments as env

my_logger = logger.get_logger(env.LOG_MODULE)


def bad_request(e):
    my_logger.debug(resp.BAD_REQUEST_400)
    return response_json(resp.BAD_REQUEST_400)    

def not_found_error(e):
    my_logger.debug(resp.SERVER_ERROR_404)
    return response_json(resp.SERVER_ERROR_404)


def internal_error(e):
    my_logger.debug(resp.SERVER_ERROR_500)
    return response_json(resp.SERVER_ERROR_500)
