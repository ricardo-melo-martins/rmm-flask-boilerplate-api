#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import Blueprint

from src.handler.response import response_json
import src.handler.response as resp

from src.utils import logger
from config import environments as env


home = Blueprint('home', __name__, url_prefix=env.API_NAMESPACE)

my_logger = logger.get_logger(env.LOG_MODULE)

@home.route('/', methods=['GET'])
def index():
    data = {'message': 'Bem vindo'}
    return response_json(resp.SUCCESS_200, data)    
