#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import Blueprint

from src.handler.response import response_json
import src.handler.response as resp
from config import environments as env

healthycheck = Blueprint('healthycheck', __name__, url_prefix=env.API_NAMESPACE)

@healthycheck.route('/ack', methods=['GET'])
def index():
    data = {'message': 'Healthy'}
    return response_json(resp.SUCCESS_200, data)