# RMM Flask Boilerplate API

# Requerimentos

- Python 3.x
- Flask 2.x
- Docker +18.06 (Opcional)
## Estrutura de diretórios e arquivos

Tenho pessoalmente um padrão de diretórios e arquivos que costumo utilizar independente da tecnologia para assim manter um padrão e facilitar lembranças dos locais. 

Claro que algum nome pode ser reservado à uma determinada tecnologia então procuro não gerar conflitos. 

```bash

├── .docker/
│   └── dockerfile
├── bin/
│   ├── rmm/
│   │   ├── install.sh
│   │   ├── start.sh
│   │   ├── logs.sh
│   │   └── clean.sh
├── config/
│   └── environment.py        
├── public/
├── src/
│   ├── controller/
│   │   ├── healthcheck.py
│   │   ├── errors.py
│   │   └── home.py
|   ├── handler/
│   │   └── response.py
|   ├── routes/
│   │   └── routes.py
|   ├── services/
│   │   └── auth.py
|   └── utils/
│       └── logger.py
├── storage/
│   └── uploads/ 
├── tmp/
|   ├── cache/
|   ├── indexes/
|   ├── locales/
|   ├── session/
│   └── logs/
├── .gitignore
├── app.py
├── requirements.txt
├── run.py
└── .gitignore
```
## Instalar

```bash
$ ./bin/rmm/install.sh
```
## Run

```bash
$ ./bin/rmm/start.sh
```

or

```bash
$ flask run
```

## Logs

```bash
$ ./bin/rmm/logs.sh
```

### Limpando Logs e Cache

Este comando deve remover cache do Python, Cache, Logs e Sessões da aplicação

```bash
$ ./bin/rmm/clear.sh
```


## Licença

É gratuito sob licença MIT e para mais informações veja [aqui](LICENSE.md).

Com :heart: por [Ricardo Melo Martins](https://gitlab.com/ricardo-melo-martins).