#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import Flask
from config import environments as env

app = Flask(__name__)

from src.routes import routes

if __name__ == "__main__":
    app.run(
        port=env.HOST_PORT, 
        host=env.HOST_ADDRESS, 
        use_reloader=False)