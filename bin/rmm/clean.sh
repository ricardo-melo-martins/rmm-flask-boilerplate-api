#!/usr/bin/env bash

source $(dirname $(readlink -fm $0))/common

echo "Removendo Logs" 
rm -rf $PATH_LOGS/*.log

echo "Removendo cache do Python"
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf