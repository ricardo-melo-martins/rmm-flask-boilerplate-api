#!/usr/bin/env bash

source $(dirname $(readlink -fm $0))/common

FILE_INSTALL="${PATH_APP_ROOT}/.install/requirements.txt" 
FILE_INSTALL_DEV="${PATH_APP_ROOT}/.install/requirements_dev.txt" 

echo "Instalando Requisitos"

if [[ ! -f $FILE_INSTALL ]] ; 
then
  echo "Arquivo [$FILE_INSTALL] de instalação não encontrado!"
  exit
else
  pip3 install -r $FILE_INSTALL  
fi

echo "Instalando Requisitos para desenvolvimento"

if [[ ! -f $FILE_INSTALL_DEV ]] ; 
then
  echo "Arquivo [$FILE_INSTALL_DEV] de instalação não encontrado!"
  exit
else
  pip3 install -r $FILE_INSTALL_DEV  
fi
