#!/usr/bin/env bash

# Comando para apresentar todos os logs
# ex. logs.sh -a debug.log error.log

# Comando para pesquisa uma string em um log
# ex. logs.sh -f debug.log "Not found"

source $(dirname $(readlink -fm $0))/common

trap 'trap - SIGTERM && kill 0' SIGINT SIGTERM EXIT


while getopts 'af:h' opt; do
  case "$opt" in
    
    a)
        echo "Apresentando Logs"

        for file in "$@"
        do	
            FILE="${PATH_LOGS}/${file}"
            
            if [[ -f "$FILE" ]]; then
                tail -f  $FILE & > 2  
            fi
        done
      ;;

    f)
        echo "Pesquisando por ${3} no arquivo ${2}"
        
        while true; 
        do 

            if [[ $1 && $2 ]]; then
                cat "${PATH_LOGS}/$2" | grep "$3" | tail -n 10; sleep 1;
            fi

        done 
      ;;
   
    ?|h)
      echo "Usage:"
      echo " $(basename $0) -f debug.log \"not found\"  Pesquisa por um texto em um log"
      echo " $(basename $0) -a debug.log error.log      Apresenta todos os logs"
      exit 1
      ;;
  esac
done

shift "$(($OPTIND -1))"

wait
